<?php

/**
 * @file
 */

function rules_letter_admin_settings($form, &$form_state) {
  
  $form['rules_letter_template_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Template Path'),
    '#default_value' => variable_get('rules_letter_template_path', 'private://letters/templates'),
    '#required' => TRUE,
  );
  $form['rules_letter_batch_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Batch Path'),
    '#default_value' => variable_get('rules_letter_batch_path', 'private://letters/batch'),
    '#required' => TRUE,
  );
  
  return system_settings_form($form);
}

function rules_letter_admin_settings_validate($form, &$form_state) {
	if (!file_prepare_directory($form_state['values']['rules_letter_template_path'], FILE_CREATE_DIRECTORY|FILE_MODIFY_PERMISSIONS)) {
		form_set_error('rules_letter_template_path', t('Unable to set up template directory'));
	}
	if (!file_prepare_directory($form_state['values']['rules_letter_batch_path'], FILE_CREATE_DIRECTORY|FILE_MODIFY_PERMISSIONS)) {
		form_set_error('rules_letter_admin_batches', t('Unable to set up batch directory'));
	}
	
}

function rules_letter_admin_batches() {
  $headers = array(
    'bid' => array(
      'data' => t('Id'),
      'field' => 'b.bid',
      'sort' => 'desc',
    ),
    'status' => array(
      'data' => t('Status'),
      'field' => 'b.status',
    ),
    'created' => array(
      'data' => t('Created'),
      'field' => 'b.created',
    ),
    'operations' => array(
      'data' => '',
    ),
  );
  $rows = array();
  
  $select = db_select('rules_letter_batch', 'b')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->fields('b', array('bid', 'created', 'uid', 'fid', 'status'))
    ->orderByHeader($headers)
    ->limit(50)
    ->execute();
  
  foreach ($select as $batch) {
    if ($batch->fid) {
      $batch->file = file_load($batch->fid);
      $links = array();
      $links['download'] = array(
        'title' => t('Download'), 
        'href' => file_create_url($batch->file->uri),
      );
    }
    $rows[] = array(
      $batch->bid,
      ($batch->status == 0 ? t('Started') : ($batch->status == 1 ? t('Generated') : ($batch->status == 2 ? t('Downloaded') : ''))),
      format_date($batch->created, 'short'),
      !empty($links) ? theme('ctools_dropdown', array('links' => $links, 'title' => t('Operations'))) : '',
    );
  }
  
  $params = array(
    'header' => $headers,
    'rows' => $rows,
  );
  
  return theme('table', $params) . theme('pager');
}