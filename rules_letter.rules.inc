<?php
/**
 * @file
 */

/**
 * Implements hook_rules_action_info().
 */
function rules_letter_rules_action_info() {
  ctools_include('export');
  $items = array();
  
  $letters = ctools_export_crud_load_all('rules_letter_templates');
  
  foreach ($letters as $letter) {
    $fields = rules_letter_get_fields($letter->pdf);
    $paramters = array();
    
    foreach ($fields as $name => $field) {
      $paramters[$name] = array(
        'type' => 'text',
        'label' => $field['name'],
        'optional' => TRUE,
      );
    }
    
    $items['rules_letters_' . $letter->template] = array(
      'label' => t('Create letter @name', array('@name' => $letter->name)),
      'group' => t('Letters'),
      'parameter' => $paramters,
      'base' => 'rules_letters_rule_apply_template',
      'template' => $letter->template,
      'provides' => array(
        'file' => array(
          'type' => 'file',
          'label' => t('The created file object.'),
        ),
        'file_uri' => array(
          'type' => 'uri',
          'label' => t('URI of created file.'),
        ),
      ),
    );
  }
  
  return $items;
}

function rules_letters_rule_apply_template() {
  global $user;

  $args = func_get_args();
  $method = array_pop($args);
  $action = array_pop($args);
  $state = array_pop($args);
  $variables = array_pop($args);
  $info = $action->info();
  
  $template = rules_letter_template_load($info['template']);
  $fields = rules_letter_get_fields($template->pdf);
  
  $values = array();
  foreach ($info['parameter'] as $name => $field) {
    $value = array_shift($args);
    
    $values[$name] = iconv("UTF-8", "ISO-8859-1", $value);
  }

  // Create the filename.
  $letter_filename = mt_rand() . '.pdf';
  // Create the file uri.
  $letter_file_uri = file_create_filename($letter_filename, 'private://letters');

  // Process the template into the file uri.
	pdftk::factory('fill_form')
	  ->setInputFile(array("filename" =>  drupal_realpath($template->pdf->uri)))
	  ->setFieldData($values)
		->setFlattenMode(true)
    ->setOutputFile(drupal_realpath($letter_file_uri))
    ->_renderPdf();

  // Save the file as a temporary managed file.
  $file = new stdClass;
  $file->uid = $user->uid;
  $file->filename = $letter_filename;
  $file->uri = $letter_file_uri;
  $file->filemime = 'text/plain';
  $file->filesize = filesize($letter_file_uri);
  $file->status = 0;
  file_save($file);

//  if ($batch = & batch_get()) {
//    if (!$batched_file = rules_letter_batch_load($batch['id'])) {
//      $batched_file = rules_letter_batch_save($batch['id']);
//    }
//
//    if ($batched_file->file->filesize) {
//      // We need to concate the new letter onto the end of the existing letter.
//      $dest = tempnam(file_directory_temp(), 'rlt');
//      pdftk::factory('cat')
//        ->setInputFile(array('filename' => drupal_realpath($batched_file->file->uri)))
//        ->setInputFile(array('filename' => $tn))
//        ->setOutputFile($dest)
//        ->_renderPdf();
//
//      file_unmanaged_move($dest, $batched_file->file->uri, FILE_EXISTS_REPLACE);
//      file_unmanaged_delete($dest);
//      file_unmanaged_delete($tn);
//    }
//    else {
//      // The file size is 0, so move first page onto the batched_file
//      file_unmanaged_move($tn, $batched_file->file->uri, FILE_EXISTS_REPLACE);
//    }
//    file_save($batched_file->file);
//    return array('file_path' => 'foo', 'file' => $batched_file->file);
//  }

  return array('file_uri' => $letter_file_uri, 'file' => $file);
}
